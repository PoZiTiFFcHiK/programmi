#include <iostream>

using namespace std;

class Matrica {
    int **A;
    int N, M;
public:
    Matrica() {
        int i;
        cout << "Vvedite M i N" << endl;
        cin >> M >> N;
        
        A = new int*[M];
        for (i = 0; i < M; i++)
            A[i] = new int[N];
    }
    
    ~Matrica() {
        int i;
        
        for (i = 0; i < M; i++)
            delete [] A[i];
        delete [] A;
    }
        
    void inMatrica() {
        int i, j, l, d;
            
        l = 0;
        d = 0;
        i = 0;
        j = 0;
            
        while (d < N)
            if (d % 2 == 0) {
                d = d + 1;
                chetD(i, j, l, d);
            }
            else {
                d = d + 1;
                nechetD(i, j, l ,d);
            }
        
        d = 0;
        l = N * M;
        i = N - 1;
        j = M - 1;
        
        while (d < N - 1)
            if (d % 2 == 0) {
                d = d + 1;
                chetD1(i, j, l, d);
            }
            else {
                d = d + 1;
                nechetD1(i, j, l, d);
            }
            
                    
                //A[i][j] = l;
           // }
    }
    
    void outMatrica() {
        int i, j;
        
        for (i = 0; i < M; i ++) {
            cout << endl;
            for (j = 0; j < N; j++)
                cout << A[i][j] << "\t";
        }
        cout << endl;
    }
private:
    void chetD(int &i, int &j, int &l, int d) {
        int k;
        for (k = 0; k < d; k++) {
            l = l + 1;
            A[i][j] = l;
            i = i + 1;
            if (j != 0)
                j = j - 1;
        }
    }
    
    void nechetD(int &i, int &j, int &l, int d) {
        int k;
        for (k = 0; k < d; k++) {
            l = l + 1;
            A[i][j] = l;
            if (i != 0)
                i = i - 1;
            j = j + 1;
        }
    }
    
    void chetD1(int &i, int &j, int &l, int d) {
        int k;
        for (k = 0; k < d; k++) {
            A[i][j] = l;
            l = l - 1;
            i = i - 1;
            if (j != N - 1)
                j = j + 1;
        }
    }
    
    void nechetD1(int &i, int &j, int &l, int d) {
        int k;
        for (k = 0; k < d; k++) {
            A[i][j] = l;
            l = l - 1;
            if (i != M - 1)
                i = i + 1;
            j = j - 1;
        }
    }
};

int main () {
    
    Matrica obj;
    
    obj.inMatrica();
    
    obj.outMatrica();
    
    
    //delete obj;
    system("pause");
    return 0;
}
